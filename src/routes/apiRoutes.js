const router = require('express').Router()
const docenteController = require('../controller/docenteController')
const signInController = require('../controller/signInController')
const userController = require('../controller/userController')
const cursoController = require('../controller/cursoController')
const turmaController = require('../controller/turmaController')

//Docente
router.post('/docente/', docenteController.postDocente)
router.get('/docente/', docenteController.getDocentes)
router.put('/docente/', docenteController.updateDocente)
router.delete('/docente/:id', docenteController.deleteDocente)

//Usuário
router.post('/user/', userController.postUser)
router.get('/user/', userController.getUsers)
router.put('/user/', userController.updateUser)
router.delete('/user/:id', userController.deleteUser)
//Login
router.post('/sign-in/', signInController.postSignIn)

//Curso
router.post('/curso/',cursoController.postCurso)
router.get('/curso/',cursoController.getCursos)
router.put('/curso/', cursoController.updateCurso)
router.delete('/curso/:id', cursoController.deleteCurso)

//Turma
router.post('/turma/', turmaController.postTurma)
router.post('/turma/novaAtribuicao/', turmaController.atribuirAula)
router.patch('/turma/removerAtribuicao/', turmaController.desatribuirAula)
router.get('/turma/',turmaController.getTurmas)
router.put('/turma/', turmaController.updateTurma)
router.delete('/turma/:id', turmaController.deleteTurma)

module.exports = router
