const express = require("express");
const app = express();
const cors = require('cors');
const db = require('./db/conn')
db();

class AppController {
  constructor() {
    this.express = express();
    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.express.use(express.json());
    this.express.use(cors())
  }
  routes() {
    const apiRoutes= require('./routes/apiRoutes')
    this.express.use('/class-project-senai-franca/api/v1/', apiRoutes);
    this.express.get("/health/", (_, res) => {
      res.send({ status: "APLICAÇÃO NO AR" });
    });
  }
}

module.exports = new AppController().express;
