const express = require('express');
const Docente = require('../modules/Docente');
const Turma = require('../modules/Turma');

module.exports = async function removeTeachingClass(turmaId,docenteId) {
    try {
        // Encontrar o docente pelo ID
        const docente = await Docente.findOne({ _id: docenteId });
        // Encontrar a turma pelo ID
        const turma = await Turma.findOne({ _id: turmaId });

        // Verificar se o docente existe
        if (!docente) {
          return {
            success: false,
            message: "Docente não encontrado."
          };
        }
        // Verificar se a turma existe
        if (!turma) {
            return { success: false, message: "Turma não encontrada." };
        }
      
        // Removendo a turma da lista de turmas do docente
        docente.turmas = docente.turmas.filter(
          (turma) => turma.toString() !== turmaId
        );
        await docente.save();

        // Atualizando a carga Horaria do docente
        let aula = turma.docentes.filter(
            (doc) => doc.docente.toString() === docenteId
          );
        docente.cargaHorariaLivre += aula[0].cargaHorariaAula;
        await docente.save();

        // Removendo o docente da lista de docentes da turma
        turma.docentes = turma.docentes.filter(
          (doc) => doc.docente.toString() !== docenteId
        );
        await turma.save();
      
        return {
          success: true,
          message: "Turma removida do docente com sucesso."
        };
      } catch (error) {
        console.error('Erro durante a remoção da turma do docente:', error);
        return {
          success: false,
          message: "Erro durante a remoção da turma do docente."
        };
      }
      
}