const express = require("express");
const Docente = require("../modules/Docente");
const Turma = require("../modules/Turma");

module.exports = async function updateTeachingClass(
  turmaId,
  diaSemana,
  turno,
  idDocente,
  siglaMateria
) {
  try {
    // Validando quantos dias iremos atribuir
    const numDias = diaSemana.length;

    // Buscando docente
    const docente = await Docente.findOne({ _id: idDocente });
    // Buscando a turma específica
    const turma = await Turma.findOne({ _id: turmaId });

    // Variável para o cálculo de horas ocupadas
    let valorAcumulado = 0;

    // Percorrendo a lista de turno para somar as horas de cada turno
    turno.forEach((turnoAtual) => {
      // Verificando as condições e atualizando o valor acumulado
      if (turnoAtual >= 1 && turnoAtual <= 4) {
        valorAcumulado += 2;
      } else if (turnoAtual === 5 || turnoAtual === 6) {
        valorAcumulado += 1.5;
      }
    });
    let horasAtribuir = numDias * valorAcumulado;
    if (docente.cargaHorariaLivre < horasAtribuir) {
      return { success: false, message: "Carga horária insuficiente." };
    } else {
      // Atualizando a carga Horaria
      docente.cargaHorariaLivre -= horasAtribuir;
      await docente.save();
      // Adicionando a nova turma ao array turmas do Docente
      docente.turmas.push(turmaId);
      await docente.save();

      // Adicionando o novo objeto ao array docentes da Turma
      turma.docentes.push({
        docente: idDocente,
        turno: turno,
        diaSemana: diaSemana,
        cargaHorariaAula: horasAtribuir,
        siglaMateria: siglaMateria,
      });
      // Salvando as alterações na turma
      await turma.save();
      return { success: true, message: "Aula atribuida com sucesso." };
    }
  } catch (error) {
    console.error("Erro ao atualizar a turma:", error);
    return { success: false, message: "Erro ao atualizar a turma." };
  }
};
