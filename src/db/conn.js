/* importando as funcionalidades do mongo */
const mongoose = require("mongoose")

/*Função de conexão com mongo*/

async function main(){
  try {
    await mongoose.connect(
      "mongodb://127.0.0.1:27017"); 
    console.log('Banco conectado')
  }
  catch(error){
    console.log('Erro: '+error)
  }
}
module.exports = main