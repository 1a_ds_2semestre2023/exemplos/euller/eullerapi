const bcrypt = require("bcrypt");
const Docente = require("../modules/Docente");
const Turma = require("../modules/Turma");
module.exports = class teacherController {
  // Cadastrar Docente
  static async postDocente(req, res) {
    const { nome, cargaHorariaTotal } = req.body;
    // Validando se há algum campo em branco
    if (cargaHorariaTotal !== 0 && nome !== "") {
      // Criando o docente no banco de dados
      Docente.create({
        nome: nome,
        cargaHorariaTotal: cargaHorariaTotal,
        cargaHorariaLivre: cargaHorariaTotal,
        turmas: [],
      });
      return res.status(201).json({ message: "Cadastrado" });
    } else {
      return res
        .status(205)
        .json({ message: "Favor preencher todos os campos" });
    }
  }

 // Listar Docentes com turmas filtradas
static async getDocentes(req, res) {
  try {
    const docentes = await Docente.find().exec();

    const docentesComTurmas = await Promise.all(
      docentes.map(async (docente) => {
        const turmas = await Turma.find({
          "docentes.docente": docente._id,
        })
          .populate("docentes.docente")
          .exec();

        // Filtrar turmas para incluir apenas o docente específico
        const turmasFiltradas = turmas.map((turma) => {
          const docenteAssociado = turma.docentes.find(
            (docenteAssociado) => docenteAssociado.docente._id.toString() === docente._id.toString()
          );
          return {
            ...turma.toObject(),
            docentes: [docenteAssociado],
          };
        });

        return {
          ...docente.toObject(),
          turmas: turmasFiltradas,
        };
      })
    );

    res.status(200).json({ docentes: docentesComTurmas });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Erro ao buscar docentes." });
  }
}

  // Atualizar Docentes
  static async updateDocente(req, res) {
    const { nome, cargaHorariaTotal,_id } = req.body;
    const updateStatus = await Docente.updateOne(
      { _id: _id },
      { $set: { nome: nome, cargaHorariaTotal: cargaHorariaTotal } }
    );
    res.status(200).json({ message: updateStatus });
  }

  // Apagar Docentes
  static async deleteDocente(req, res) {
    try {
      // Verificando se o docente está vinculado a alguma turma
      const turmasComDocente = await Turma.find({ 'docentes.docente': req.params.id });

      if (turmasComDocente.length > 0) {
        // Se o docente estiver vinculado a alguma turma, remover da lista de docentes nas turmas
        await Promise.all(
          turmasComDocente.map(async (turma) => {
            turma.docentes = turma.docentes.filter((doc) => doc.docente.toString() !== req.params.id);
            await turma.save();
          })
        );
      // Excluir docente 
      const deleteStatus = await Docente.deleteOne({ _id: req.params.id });
      if (deleteStatus.deletedCount > 0) {
        return res.status(200).json({ message: 'Docente excluído com sucesso. As turmas atribuidas foram atualizadas.'});
      } else {
        return res.status(404).json({ message: 'Docente não encontrado.' });
      }
      }
      else{
     // Excluir docente 
      const deleteStatus = await Docente.deleteOne({ _id: req.params.id });
      if (deleteStatus.deletedCount > 0) {
        return res.status(200).json({ message: 'Docente excluído com sucesso.' });
      } else {
        return res.status(404).json({ message: 'Docente não encontrado.' });
      }

      }
    } catch (error) {
      console.error('Erro ao excluir docente:', error);
      return res.status(500).json({ message: 'Erro ao excluir docente.' });
    }
  }
};
