const User = require("../modules/User");
const bcrypt = require("bcrypt");

module.exports = class userController {
  // Cadastrar Usuários
  static async postUser(req, res) {
    const { sn, password, email } = req.body;
    let hash = bcrypt.hashSync(password, 10);
    User.create({
      sn: sn,
      password: hash,
      email: email,
    });

    res.status(200).json({ message: "Cadastrado" });
  }

  //Listar Usuários
  static async getUsers(req, res) {
    const users = await User.find();
    res.status(200).json({ users });
  }

  // Atualizar Usuário
  static async updateUser(req, res) {
    const { sn, password, email, _id } = req.body;
    const updateStatus = await User.updateOne(
      { _id: _id },
      { $set: 
        { email: email, 
          password: password, 
          sn: sn 
        } 
      }
    );
    console.log(updateStatus)
    res.status(200).json({ message:updateStatus });
  }

  // Apagar Usuário
  static async deleteUser(req, res) {
    const deleteStatus = await User.deleteOne({ _id: req.params.id });
    if(deleteStatus.deletedCount>=1){
      return res.status(200).json({ message:deleteStatus });
    }
    else{
      return res.status(200).json({ message:'Usuário não encontrado' });
    }
    
  }
};
