const User = require("../modules/User");
const bcrypt = require("bcrypt");

module.exports = class signInController {
    // Autenticar User
    static async postSignIn(req, res) {
      const { sn, password } = req.body;
      const auth = await User.findOne({ sn: sn });
  
      if (auth != null) {
        if (bcrypt.compareSync(password, auth.password) && sn === auth.sn) {
          res.status(200).json({ message: "Autenticado" , status:true});
        } else {
          res.status(200).json({ message: "Senha incorreta" , status:false});
        }
      } else {
        res.status(400).json({ message: "Usuário não cadastrado" , status:false});
      }
    }
  };