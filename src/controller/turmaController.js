const Turma = require("../modules/Turma");
const updateTeachingClass = require("../services/updateTeachingClass")
const removeTeachingClass = require("../services/removeTeachingClass")

module.exports = class turmaController {
  // Cadastrar Turma
  static async postTurma(req, res) {

    const { duracaoAula, periodo, status, curso, dateInit, dateEnd,diaSemana,nome } = req.body;
    // Validando se há algum campo em branco
    if (periodo !== "" && duracaoAula !== 0 && curso !== null && diaSemana.length>0) {
      // Criando turma no banco de dados
      Turma.create({
        nome:nome,
        duracaoAula: duracaoAula,
        periodo: periodo,
        status: status,
        dateInit:dateInit,
        dateEnd:dateEnd,
        curso: curso._id,
        diaSemana:diaSemana
      });
      return res.status(201).json({ message: "Turma cadastrada" });
    }
    else{
      return res.status(205).json({ message: "Favor preencher todos os campos" });
    }
  }

  //Listar Turmas
  static async getTurmas(req, res) {
    const turmas = await Turma.find().populate("docentes.docente", "nome");
    res.status(200).json({ turmas });
  }

  // Atualizar Turma
  static async updateTurma(req, res) {
    const { duracaoAula, periodo, status, curso, dateInit, dateEnd,diaSemana, _id, nome } = req.body;
    const updateStatus = await Turma.updateOne(
      { _id: _id },
      { $set: 
        { duracaoAula: duracaoAula,
          periodo: periodo,
          status: status,
          dateInit:dateInit,
          dateEnd:dateEnd,
          curso: curso,
          diaSemana:diaSemana,
          nome:nome
        } 
      }
    );
    res.status(200).json({ message:updateStatus });
  }

  // Apagar Turma
  static async deleteTurma(req, res) {
    const deleteStatus = await Turma.deleteOne({ _id: req.params.id });
    if(deleteStatus.deletedCount>=1){
      return res.status(200).json({ message:deleteStatus });
    }
    else{
      return res.status(200).json({ message:'Turma não encontrada' });
    }
    
  }

// Adicionar um docente a lista
static async atribuirAula(req, res) {
  const { idDocente, turno, diaSemana, siglaMateria, turmaId } = req.body;
  if(idDocente!==""&& turno.length>=1 && diaSemana.length>=1 && siglaMateria!==""){
  let newTeacherClass = await updateTeachingClass(turmaId,diaSemana,turno,idDocente,siglaMateria)
  if(newTeacherClass.success===true){
    return res.status(200).json({ message:newTeacherClass.message });
  }
  else{
    return res.status(400).json({ message:newTeacherClass.message });
  }
}
else{
  return res.status(400).json({ message:'Preencha todos os campos' });
}
}

// Removendo um docente da lista
static async desatribuirAula(req, res) {
  const {turmaId , docenteId} =  req.body
  let removeTeacherClass = await removeTeachingClass(turmaId , docenteId)
  if (removeTeacherClass.success === true) {
    return res.status(200).json({ message: removeTeacherClass.message });
  } else {
    console.log(removeTeacherClass)
    return res.status(400).json({ message: removeTeacherClass.message });
  }
}

};
