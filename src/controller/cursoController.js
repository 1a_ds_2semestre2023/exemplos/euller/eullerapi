const Curso = require("../modules/Curso");
const Turma = require("../modules/Turma");

module.exports = class cursoController {
  // Cadastrar Curso
  static async postCurso(req, res) {
    const { tipo, nome, cargaHoraria, sigla } = req.body;
    // Validando se há algum campo em branco
    if (tipo !== "" && nome !== "" && cargaHoraria !== 0 && sigla !== "") {
      // Criando curso no banco de dados
      Curso.create({
        tipo: tipo,
        nome: nome,
        cargaHoraria: cargaHoraria,
        sigla: sigla

      });
      return res.status(201).json({ message: "Cadastrado" });
    }
    else{
      return res.status(205).json({ message: "Favor preencher todos os campos" });
    }
  }

  //Listar Cursos
  static async getCursos(req, res) {
    const cursos = await Curso.find();
    res.status(200).json({ cursos });
  }

  // Atualizar Curso
  static async updateCurso(req, res) {
    const { tipo, nome, cargaHoraria, sigla, _id } = req.body;
    const updateStatus = await Curso.updateOne(
      { _id: _id },
      { $set: 
        { tipo: tipo,
          nome: nome,
          cargaHoraria: cargaHoraria,
          sigla: sigla
        } 
      }
    );
    res.status(200).json({ message:updateStatus });
  }

  // Apagar Curso
  static async deleteCurso(req, res) {
    try {
      // Verificar se o curso está vinculado a alguma turma
      const turmasComCurso = await Turma.find({ curso: req.params.id });

      if (turmasComCurso.length > 0) {
        return res.status(400).json({ message: 'Não é possível excluir o curso pois ele está vinculado a uma turma.' });
      }

      // Se o curso não estiver vinculado a nenhuma turma, pode ser excluído
      const deleteStatus = await Curso.deleteOne({ _id: req.params.id });

      if (deleteStatus.deletedCount > 0) {
        return res.status(200).json({ message: 'Curso excluído com sucesso.' });
      } else {
        return res.status(404).json({ message: 'Curso não encontrado.' });
      }
    } catch (error) {
      console.error('Erro ao excluir curso:', error);
      return res.status(500).json({ message: 'Erro ao excluir curso.' });
    }
  }
};
