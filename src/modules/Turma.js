const mongoose = require("mongoose");
const Curso = require("../modules/Curso");

const Turma = mongoose.model("Turma", {
  nome:String,
  duracaoAula: Number,
  periodo: String,
  status: Boolean,
  dateInit: Date,
  dateEnd: Date,
  diaSemana: [String],
  curso: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Curso",
  },
  docentes: [
    {
      docente: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Docente",
      },
      turno: [Number],
      diaSemana: [String],
      cargaHorariaAula:Number,
      siglaMateria:String
    },
  ],
});

module.exports = Turma;