const mongoose = require ('mongoose')

const Docente = mongoose.model('Docente',{
    nome: String,
    cargaHorariaTotal: Number,
    cargaHorariaLivre: Number,
    turmas: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Turma'
        }
    ]
})

module.exports = Docente