const mongoose = require ('mongoose')

const Curso = mongoose.model('Curso',{
    tipo:String,
    nome:String,
    cargaHoraria:Number,
    sigla:String
})

module.exports = Curso